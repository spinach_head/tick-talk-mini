import request from '@/utils/http.js'

//登录
export const login = params => request('post','bottle/login',params)

//设置个人信息
export const setUserInfo = params => request('post','bottle/setUserInfo',params)
//获取个人信息
export const getUserInfo = params => request('get','bottle/getUserInfo',params)
//扔瓶子
export const sendPaper = params => request('post','bottle/sendPaper',params)
//捡瓶子
export const pickUpBottle = params => request('get','bottle/pickUpBottle',params)
//回复瓶子 ReplyPaper
export const replyPaper = params => request('post','bottle/replyPaper',params)
//获取两人之间的漂流瓶 
export const getPaperContent = params => request('get','bottle/getPaperContent',params)
//设置用户地址信息
export const setUserAddress = params => request('post','bottle/setUserAddress',params)
//获取可捡和扔瓶子的数量
export const getPickAndThrowNum = params => request('get','bottle/getPickAndThrowNum',params)
//通过uuid获取用户信息
export const getUserInfoByUUID = params => request('get','bottle/getUserInfoByUUID',params)
//举报用户
export const reportUser =  params => request('post','bottle/reportUser',params)
//拉黑用户
export const addBlacklist =  params => request('post','bottle/addBlacklist',params)
//是否已经拉黑该用户
export const isBlackUser =  params => request('post','bottle/isBlackUser',params)
//取消拉黑用户
export const delBlacklist =  params => request('post','bottle/delBlacklist',params)
//查询拉黑的用户
export const queryBlacklist =  params => request('get','bottle/queryBlacklist',params)
//获取消息列表
export const getConversations =  params => request('get','bottle/getConversations',params)
//是否需要机器人发送消息
export const isNeedAIMessage = params => request('get', 'bottle/isNeedAIMessage', params)
//双方是否有一人拉黑对方
export const isBlackOther = params => request('post', 'bottle/isBlackOther', params)
//获取七牛认证
export const getQiNiuAuthor = params => request('get', 'bottle/getQiNiuAuthor', params)
