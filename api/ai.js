import otherRequest from "../utils/otherRequest.js"

let aiUrl = 'https://dashscope.aliyuncs.com/api/v1/apps/db9d44a4af6f4e0ca8939a4aa03dee4e/completion'
let aiHeader = {
	'Content-type': 'application/json',
	'Authorization': 'sk-1de289eb5e8a4610a51d45d8cd5f03a8'
}
let imMessageUrl = 'https://rest-hz.goeasy.io/v2/im/message'
let ossImageUrl = 'https://name.jiaoguanyi.cn/api/getOssImgInfo'
export const talkAI = params => otherRequest('post',aiUrl,params, aiHeader)

export const imMessage = params => otherRequest('post', imMessageUrl, params)

export const getOssImgInfo = params => otherRequest('post', ossImageUrl, params)