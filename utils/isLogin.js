const isLogin = () => {
    return new Promise((resolve, reject) => {
		console.log("这是userId", uni.getStorageSync("userId"))
        if (!uni.getStorageSync("userId")) {
			console.log("请先进行登录")
            uni.showLoading({
                title: '请先登录',
                success(res) {
                    uni.switchTab({ url: "/pages/userHome/userHome" });
                    uni.hideLoading()
                }
            })
           reject('未登录');
        } else {
            resolve('已登录');
        }
    })
}

export {
    isLogin
}