import {
	login
} from '../api/login.js';
import {
	promisify,
	request
} from './http.js';
// 是否登录开发者服务端
let isDeveloperServerLogin = false;
// 是否登录抖音主端
let isDouyinLogin = false;
class LoginController {

	_token = '';
	userId = '';

	/**
	 * @description: 登录函数，首先调用tt.login获取code，并向开发者服务端请求登录
	 * @param {*} force 当用户未登录抖音app时，是否强制调起抖音app登录界面
	 * @return {*}
	 */
	async login(force = true) {
		if (isDeveloperServerLogin) {
			return;
		}
		let loginData = null;
		try {
			// 是否强制调起抖音的登录窗口
			loginData = await promisify(tt.login, {
				force
			});
			isDouyinLogin = loginData.isLogin;
		} catch (err) {
			console.error(err);
			tt.showToast({
				icon: 'fail',
				title: '开发者取消登录抖音',
			});
		}
		if (isDouyinLogin && loginData) {
			try {
				// 与开发者服务端交互，进行登录
				await this.loginToDeveloperServer(loginData.code);
				isDeveloperServerLogin = true;
			} catch (err) {
				console.error(err);
				tt.showToast({
					title: '授权登录失败',
					icon: 'fail',
				});
			}
		}
		return isDouyinLogin;
	}
	
	async wxLogin() {
		let that = this
		wx.login({
			success(res) {
				if (res.code) {
					try {
						that.loginToDeveloperServer(res.code);
						isDeveloperServerLogin = true
					} catch(err) {
						console.log(err);
						uni.showToast({
							title: '授权登录失败',
							icon: "fail"
						})
					}
				}else {
					console.log('登录失败! ' + res.errMsg)
				}
			},
			fail(err) {
				console.log("微信登录接调取失败", err)
			}
		})
	}

	/**
	 * @description: 请求开发者服务端，做code2session
	 * @param {string} code tt.login返回的code
	 * @return {*}
	 */
	async loginToDeveloperServer(code) {
		try {
			
			uni.showLoading({
				title: '正在登录开发者服务端'
			})

			await login({
				code: code
			}).then(result => {
				let res = result.data
				console.log("这是res", res)
				console.log("这是res.user", res.user)
				this._token = res.token;
				this.userId = res.user.id
				uni.setStorageSync('token', this._token);
				uni.setStorageSync('userId', this.userId);
				uni.setStorageSync('uuid', res.user.uuid)
			})

		} catch (err) {
			console.log(err);
		} finally {
			uni.hideLoading()
		}
	}

	/**
	 * @description: 登出开发者服务端
	 * @return {*}
	 */
	async logout() {
		await request({
			method: 'post',
			url: '/api/apps/logout',
			data: {
				token: loginController.getToken(),
			},
		});
		isDeveloperServerLogin = false;
		uni.removeStorageSync('token');
		uni.removeStorageSync('userId');
		uni.removeStorageSync('uuid');
	}

	/**
	 * @description: 返回是否登录抖音主端
	 * @return {*}
	 */
	isDouyinLogin() {
		return isDouyinLogin;
	}

	/**
	 * @description: 是否登录开发者服务端
	 * @return {*}
	 */
	isDeveloperServerLogin() {
		return isDeveloperServerLogin;
	}

	/**
	 * @description: 获取localStorage中的token
	 * @return {string}
	 */
	getToken() {
		return this._token || tt.getStorageSync('token');
	}

}
export const loginController = new LoginController();