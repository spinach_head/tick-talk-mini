// index.js
const cloud = require('wx-server-sdk')
cloud.init({
	env: 'spinachead-bottle-4e3ldb260b193a',
})
const db = cloud.database()
exports.main = async (event, context) => {
	return db.collection('user').get()
}